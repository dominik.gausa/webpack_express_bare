import * as express from 'express';

const app = express();

app.all('/', (req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.send('root');
})
app.all('/hi', (req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.send('hi');
})

const routeAPI = express.Router();
routeAPI.all('/', (req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.send('api');
})
routeAPI.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.send('API undefined');
})

app.use('/api', routeAPI);

app.listen(9001, () => {
  console.log('Hello World');
})