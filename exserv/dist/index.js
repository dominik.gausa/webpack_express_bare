"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var app = express();
app.all('/', function (req, res, next) {
    res.send('root');
});
app.all('/hi', function (req, res, next) {
    res.send('hi');
});
var routeAPI = express.Router();
routeAPI.all('/', function (req, res, next) {
    res.send('api');
});
routeAPI.use(function (req, res, next) {
    res.send('API undefined');
});
app.use('/api', routeAPI);
app.listen(9001, function () {
    console.log('Hello World');
});
