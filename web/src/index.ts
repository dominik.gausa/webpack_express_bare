import * as _ from 'lodash';

let counter: number = 0;

let element: HTMLDivElement = document.createElement('div');
function component() {
    // Lodash, now imported by this script
    element.innerHTML = _.join(['Hey', 'webpack', counter], ' ');
    return element;
}
document.body.appendChild(component());

setInterval(() => {
    counter++;
    component();
}, 300);